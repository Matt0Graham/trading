package com.citi.training.trading.dao;

import java.util.List;

import com.citi.training.trading.model.Trade;

/**
 * An data object interface for the trade table
 * @author Administrator
 *
 */
public interface TradeDao {
	List<Trade> findAll();
	Trade findById(int id);
	Trade create(Trade trade);
	void deleteById (int id);
}
