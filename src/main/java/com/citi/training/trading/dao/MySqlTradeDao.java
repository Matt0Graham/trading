package com.citi.training.trading.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trading.exception.TradeNotFoundException;
import com.citi.training.trading.model.Trade;

@Component
/**
 * Trade data object that queries the SQL database
 * 
 * @author Administrator
 *
 */
public class MySqlTradeDao implements TradeDao {

	@Autowired
	JdbcTemplate tpl;

	/**
	 * Returns a list of type {@link com.citi.training.trading.model.Trade} for all
	 * trades in the trade table
	 * 
	 * @return List list of all trade objects
	 */
	@Override
	public List<Trade> findAll() {
		return tpl.query("SELECT * FROM trade", new TradeMapper());

	}

	/**
	 * Returns a {@link com.citi.training.trading.model.Trade} object by an its
	 * integer id
	 * 
	 * @param id The int id of the trade
	 * @return {@link com.citi.training.trading.model.Trade} the trade with that id
	 */
	@Override
	public Trade findById(int id) {

		List<Trade> trades = tpl.query("SELECT * from trade WHERE id = ?", new Object[] { id }, new TradeMapper());

		if (trades.size() > 0) {
			return trades.get(0);
		} else {
			throw new TradeNotFoundException("trade by that id does not exist");
		}

	}

	/**
	 * Creates a trade in the database using stock,price,volume member 
	 * variables of a trade object
	 * @param {@link com.citi.training.trading.model.Trade} a trade object containing the relevant values
	 * @return {@link com.citi.training.trading.model.Trade} the same object passed into the method
	 */
	@Override
	public Trade create(Trade trade) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement("INSERT INTO trade(stock,price,volume) values (?,?,?)",
						Statement.RETURN_GENERATED_KEYS);

				ps.setString(1, trade.getStock());
				ps.setDouble(2, trade.getPrice());
				ps.setInt(3, trade.getVolume());

				return ps;
			}

		}, keyHolder);
		trade.setId(keyHolder.getKey().intValue());
		return trade;
	}

	/**
	 * Deletes a trade record by its relevant id#
	 * @param int an id of a trade record
	 */
	@Override
	public void deleteById(int id) {
		findById(id);
		tpl.update("DELETE FROM trade WHERE id = ?", id);
	}

	/**
	 * An implementation of the {@link org.springframework.jdbc.core.RowMapper} class
	 * mapping columns of a result set to attributes of a trade object
	 * @return {@link com.citi.training.trading.model.Trade} trade object
	 * @author Administrator
	 *
	 */
	private static final class TradeMapper implements RowMapper<Trade> {
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("id"), rs.getString("stock"), rs.getDouble("price"), rs.getInt("volume"));
		}
	}

}
