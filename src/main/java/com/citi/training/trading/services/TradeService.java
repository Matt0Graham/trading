package com.citi.training.trading.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trading.dao.MySqlTradeDao;
import com.citi.training.trading.model.Trade;

@Component
public class TradeService {


	@Autowired
	private MySqlTradeDao tradeService;

	public List<Trade> findAll() {
		return tradeService.findAll();
	}

	public Trade findById(int id) {
		return tradeService.findById(id);
	}

	public Trade create(Trade trade) {
		if(trade.getStock().length()>0) {
			return tradeService.create(trade);			
		}else {
			throw new RuntimeException("stock name cant be empty");
		}
	}

	public void deleteById(int id) {
		tradeService.deleteById(id);
	}
}
