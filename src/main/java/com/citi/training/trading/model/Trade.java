/**
 * 
 */
package com.citi.training.trading.model;

/**
 * A object that represents a trade 
 * 
 * @author Administrator
 *
 */
public class Trade {
	private int id;
	private String stock;
	private double price;
	private int volume;
	
	/**
	 * Default constructor
	 */
	public Trade() {
		
	}
	
	/**
	 * 
	 * A constructor that accepts parameters for creating a trade object
	 * 
	 * @param id  id in trade in db 
	 * @param stock a stock symbol e.g. AAPL, GOOGL, MSFT
	 * @param price a price of a trade
	 * @param volume the amount of stock used in this trade
	 */
	public Trade(int id, String stock, double price, int volume) {
		super();
		this.id = id;
		this.stock = stock;
		this.price = price;
		this.volume = volume;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}

	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", price=" + price + ", volume=" + volume + "]";
	}
	
	
	
}
