package com.citi.training.trading.exception;

/**
 * A custom exception for handling erroneous trade requests
 * @author Administrator
 */
public class TradeNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TradeNotFoundException(String message) {
		super(message);
	}
}
