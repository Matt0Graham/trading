package com.citi.training.trading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
/**
 * Entry point for application
 * @author Administrator
 *
 */
public class TradingApplication {

	/**
	 * Main method of application
	 * @param args array of string args to be given to app
	 */
	public static void main(String[] args) {
		SpringApplication.run(TradingApplication.class, args);
	}
}
