package com.citi.training.trading.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class TradeTests {

	@Test
	public void testTradeIntStringDoubleIntAndGetters() {
		int id = 1;
		int volume = 1000;
		String stock = "MSFT";
		double price = 40.23;
		
		Trade t1 = new Trade(id,stock,price,volume);
		
		assert(t1.getId()==id);
		assert(t1.getStock()==stock);
		assert(t1.getPrice()==price);
		assert(t1.getVolume()==volume);	
	}

	

	@Test
	public void testSetters() {
		
		Trade t1 = new Trade();
		int id = 1;
		int volume = 1000;
		String stock = "MSFT";
		double price = 40.23;
		
		t1.setId(id);
		t1.setPrice(price);
		t1.setStock(stock);		
		t1.setVolume(volume);
		
		assert(t1.getId()==id);
		assert(t1.getStock()==stock);
		assert(t1.getPrice()==price);
		assert(t1.getVolume()==volume);	
	}

}
