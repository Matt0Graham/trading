package com.citi.training.trading.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trading.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MySqlTradeDaoTests {

	@Autowired
	MySqlTradeDao mySqlTradeDao;

	@Test
	@Transactional
	public void testCreateAndFindAll() {
		mySqlTradeDao.create(new Trade(1, "APL", 40.20, 200));
		assertEquals(1, mySqlTradeDao.findAll().size());
	}

	@Test
	@Transactional
	public void testFindById() {
		int id = mySqlTradeDao.create(new Trade(1, "APL", 40.20, 200)).getId();
		assert (mySqlTradeDao.findById(id).getId() == id);
	}

	@Transactional
	@Test(expected = IndexOutOfBoundsException.class)
	public void testDeleteById() {
		int id = mySqlTradeDao.create(new Trade(1, "APL", 40.20, 200)).getId();
		mySqlTradeDao.deleteById(id);
		mySqlTradeDao.findById(id);
	
	}

}
